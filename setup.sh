sudo apt update
sudo apt -y upgrade
sudo apt -y install vim git zsh mc wget htop
echo 'syntax on' > ~/.vimrc
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
chsh -s `which zsh`
